Javascript
==========

El Módulo Javascript es el primer paso para la construcción de sitios web profesionales en donde los estudiantes aprenden conceptos de programación a nivel Front-end.

Descripción:
-----------

El Módulo Javascript de Capacitadero esta dirigido a estudiantes que tienen alguna discapacidad y quieren aprender a Desarrollar sitios webs dinámicos. De igual manera, estos recursos son útiles para cualquier persona que quiera iniciarse en el Lenguaje de Programación Javascript.

Requisitos Previos:
------------------

A nivel general, no existe requisito previo para el Módulo Javascript. Sin embargo, es recomendable tener conocimiento previo de las etiquetas HTML.

Utilizaremos un Editor de Texto como Sublime Text, Atom o Bloc de Notas, según la preferencia del estudiante.

Pueden trabajar en cualquier sistema operativo.

Para estudiantes con discapacidad visual es necesario tener experiencia utilizando algún lector de pantalla para computadora: JAWS, NVDA, etc.

Tabla de contenido:
------------------


Contribución:
------------

Si quieres colaborar con Capacitadero es ideal comunicarte con nosotros porque queremos conocerte! Escribiendo a: capacitadero at gmail dot com

Licencia:
--------
Toda la documentación pertenece a Capacitadero sin embargo, buscamos aportar al mundo contenido constructivo libre de uso. Si utilizas nuestra documentación para fines positivos, estaremos felices.