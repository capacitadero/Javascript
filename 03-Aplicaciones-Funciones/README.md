Aplicaciones y Funciones
========================

Hasta el momento hemos visto conceptos y pequeños ejemplos de código Javascript. Ahora aplicaremos lo aprendido!

---

Vamos a crear un archivo HTML y Javascript llamados: **index.html** y **mycode.js**

No olvidar tener nuestro código HTMl con la estructura básica y el vinculo a **mycode.js**

Hemos aprendido que el simbolo + se utiliza para realizar sumas y tambien concatenación.

Vamos a ver un caso de suma considerando todo lo aprendido hasta hoy:

```javascript
var a = 2;
var b = 3;
var suma;
suma = a+b;
alert(suma);
```

Y ahora, en el caso de la concatenación:

```javascript
var nombre = "Maria";
var apellido = "Ortiz";
alert(nombre+" "+apellido);
```

```javascript
var first_name = "Gloria";
var last_name = "Ortiz"
var full_name = first_name+" "+last_name;
alert(full_name); 
```

Hay que recordar que tambien podemos restar, multiplicar y dividir:

```javascript
var n1 = 2;
var n2 = 3;
var rest, mult, div;
rest = n2-n1;
mult = n2*n1;
div = n2/n1;
alert(rest+" "+mult+" "+div);
```

Un detalle importante es que nuestras variables tengan nombres que esten relacionados con su contenido.

No podriamos llamar al resultado de n2-n1 como div porque no tiene relación y dificulta entender el código.

---

### Funciones:

Utilizaremos lo aprendido para crear nuestra primera funcion de Javascript.

Ya hemos utilizado una funcion llamada `alert()` ahora empezamos creando `suma()`:

```javascript
function suma(num1,num2) {
  var resultado = num1+num2;
  return resultado;
}
alert(suma(2,3));
```

La sentencia return le dice al navegador que devuelva la variable resultado fuera de la función, para que esté disponible para su uso.

Esto es necesario porque las variables definidas dentro de funciones, sólo están disponibles dentro de esas funciones.

Un ejemplo interesante es encontrar el Precio de un producto sabiendo su valor de venta.

```html
Precio = 19% del Valor de Venta + Valor de Venta.
```

El Precio es la suma del valor de venta del producto más el impuesto correspondiente(IGV 19%) del valor de venta:

```javascript
function precio(valorventa) {
	const igv = 0.19;
    var resultado = (igv*valorventa)+valorventa;
    
  return resultado;
}
alert(precio(100));
```

Como observamos, estamos utilizando `const`, es útil para declarar referencias que no se van a reasignar.

---

Ahora vamos a construir funciones que nos ayuden a obtener resultados:

### Reto1:

Elaboremos una función que nos permita obtener la potencia de un numero positivo considerando su exponente.

Ejemplo: mipotencia(numero,exponente)

### Reto2:

Construyamos una funcion que nos ayude a determinar los N primeros números enteros positivos y lo comprobaremos con la suma de los primeros 50 números.

Deberias de obtener 1275.