HTML y Javascript
=================

Creando nuestra primera web dinámica!

Objetivo:
--------

-Identificar la importancia de Javascript dentro del Desarrollo Web.

-Decodificar código Javascript.

-Crear archivo app.html y functions.js

Proceso:
-------

Qué es JavaScript?

JavaScript es un lenguaje de programación que puede ser aplicado a un documento HTML y usado para crear interactividad dinámica en los sitios web.

Puedes hacer casi cualquier cosa con JavaScript. Puedes empezar con pequeñas cosas como carruseles, galerías de imágenes, diseños fluctuantes, y respuestas a las pulsaciones de botones. Con más experiencia, serás capaz de crear juegos, animaciones 2D y gráficos 3D, aplicaciones integradas basadas en bases de datos,etc.

JavaScript es una de las tecnologías web más emocionantes, y cuando comiences a ser bueno en su uso, tus sitios web entrarán en una nueva dimensión de energía y creatividad.

Sin embargo, sentirse cómodo con JavaScript es un poco más difícil que sentirse cómodo con HTML y CSS . Deberás comenzar poco a poco y continuar trabajando en pasos pequeños y consistentes. Para comenzar, mostraremos cómo añadir JavaScript básico a tu página, creando un "¡Hola Mundo!" de ejemplo.

Creamos un archivo html llamando **app.html** , en el cual vamos a añadir la estructura ya conocida utilizada en el Taller Introductorio:

```html
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="utf-8">
    <title>Mi Primera Sesión</title>
  </head>
  <body>
    <h1>Hola Mundo!</h1>
    <p>Prueba de mi Javascript</p>    
	<script type="text/javascript">
      alert('Hola Mundo!');
    </script>
  </body>
</html>
```
Recordando:

1. `<!DOCTYPE html>`: En los inicios de HTML (1991-92), los doctypes servían como enlaces al conjunto de reglas que la página HTML debía seguir para considerarse buen HTML. En la actualidad se ignora y se considera un legado histórico que hay que incluir para que todo funcione correctamente: Todos los sitios webs 
lo utilizan como un estandar, sin embargo, si pruebas en casa todo va a funcionar igual sin él :) 

2. `<html></html>` : El elemento  `html`. Este elemento engloba todo el contenido de la página y es
conocido en ocasiones como el elemento raíz.

3. `<head></head>`: El elemento `head` (cabecera). Este elemento actúa como contenedor de información que quieras incluir en el documento HTML que **NO** será visible a los visitantes de la página.

4. `<meta charset="utf-8">`: Este elemento establece que tu documento HTML usará la codificación uft-8, que incluye la gran mayoría de caracteres de todos los lenguajes humanos conocidos. Es decir: puede tratar cualquier contenido de texto que pongas en tu documento. No hay razón para no configurarlo y te puede ayudar a evitar problemas más adelante.

5. `<title></title>`: Este elemento establece el título de tu página, que aparece en la pestaña/ventana de tu navegador cuando la página se carga.

6. `<body></body>` : El elemento `body` contiene todo el contenido que quieres mostrar a los usuarios cuando visitan tu página, ya sea texto, imágenes, vídeos, juegos, pistas de audio reproducibles o cualquier otra cosa.

7. En HTMl, para añadir comentarios a nuestro código u ocultar algunas lineas se utiliza:

`<!-- mi comentario o elementos que quiero ocultar -->`

Lo nuevo:

 `<script type="text/javascript"></script>` : Para añadir elementos Javascript en nuestro sitio web una forma es utilizar la etiqueta script de abrir y cerrar para en medio ingresar todo el código.

Si ya tenemos nuestro archivo llamado **app.html** construido podremos ir al folder o carpeta donde esta este y abrirlo para verlo desde el navegador.

Al abrirlo veremos que el navegador nos envia un mensaje, como una alerta donde nos dice: Hola Mundo!

Como podemos darnos cuenta,  `alert()` en javascript envia al usuario un cuadro de alerta con el mensaje que nosotros queremos. En el alert se puede utilizar comillas simples o dobles.

 `alert()` nos ayudara mucho dentro de nuestro proceso de aprendizaje para comprobar si todo salio bien, sin embargo utilizaremos más adelante metodos más amigables que incluiran pequeñas animaciones tipicas como los "cargando..." que hay en muchos sitios web.
 
En javascript para los comentarios u ocultar lineas de código se utiliza:

```html
<script type="text/javascript">
/*
comentarios o código que quiero ocultar
*/
</script>
```

En ocasiones, nuestro código Javascript estara escrito en muchas lineas y será necesario tenerlo en otro archivo. Vamos a ocultar todo el script utilizado en nuestro archivo **app.html**

```html
<!--
<script type="text/javascript">
      alert('Hola Mundo!');
</script>
-->
```

Ahora crearemos un nuevo archivo llamado functions.js el cual es un archivo javascript en donde escribiremos todo nuestro código.

Un detalle importante es vincular el archivo html con nuestro nuevo documento javascript.Para ello, utilizaremos el siguinte código:

```html
<script src="functions.js"></script>
```

El elemento `script` tiene un atributo llamado `src` (source en ingles) el cual significa fuente u origen y contiene la ubicación del archivo que queremos vincular.

En esta ocasión, nuestros archivos **app.html** y **functions.js** se encuentrar en la misma carpeta, por ello solo escribimos **functions.js**

(Otro ejemplo podria ser `js/functions.js` en caso nuestro documento javascript este ubicado en la carpeta `js` )

Este código estará dentro del body antes de su etiqueta de cierre. Al final tendremos un archivo asi:

```html
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="utf-8">
    <title>Mi Primera Sesión</title>
  </head>
  <body>
    <h1>Hola Mundo!</h1>
    <p>Prueba de mi Javascript</p>    
<!--    <script type="text/javascript">
	      alert('Hola Mundo!');
	    </script>      -->
  <script src="functions.js"></script>
  </body>
</html>
```
Y crearemos ahora un alert con el mensaje "Hola Mundo!" dentro de nuestro archivo **functions.js**

```javascript
alert('Hola Mundo!');
```

Hacemos una prueba y seguimos avanzando.

Recursos Adicionales:
--------------------
Parte del texto del Proceso fue extraido de la organización [Mozilla](https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/JavaScript_basics)


