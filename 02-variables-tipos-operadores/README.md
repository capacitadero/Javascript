Variables, tipos de variables y los operadores
==============================================

Empezando por la básico.

Objetivo:
--------

-Identificar los conceptos básicos de variables, tipos y operadores del Lenguaje Javascript.

-Decodificar código Javascript.

-Crear código Javascript contenido en los archivos de trabajo.

Proceso:
-------

### Variables:

Para ello, es importante conocer el concepto de Variable:

Las variables son contenedores en los que puedes almacenar valores. Primero hay que declarar la variable con la palabra clave var, seguida del nombre que le quieras dar:

var nombreDeLaVariable;

### Atención:

-Todas las lineas en JS deben acabar en punto y coma para indicar que es ahí donde termina la línea.

-Puedes llamar a una variable con casi cualquier nombre, pero hay algunas restricciones. Aquí tenemos la [lista](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Palabras_Reservadas)

-Además, hay una herramienta que puede ser útil [Aquí](https://mothereff.in/js-variables)

-JavaScript distingue entre mayúsculas y minúsculas: miVariable es una variable distinta a mivariable.

Luego de declarar la variable, hay que darle un valor:

```javascript
nombreDeLaVariable = 'capacitadero';
```

Estas dos lineas se pueden resumir tambien en una sola:

```javascript
var nombreDeLaVariable = 'capacitadero';
```

Vamos a llamar a la variable creada por medio de un alert:

```javascript
alert(nombreDeLaVariable);
```

En resumen, dentro de nuestro archivo JS tendremos nuestro código asi:

```javascript
var nombreDeLaVariable;
nombreDeLaVariable = 'capacitadero';
/*
en una sola linera seria:
var nombreDeLaVariable = 'capacitadero';
*/
alert(nombreDeLaVariable);
```

---


### Tipo de Datos:

Cada variable puede ser de algún tipo de dato, en JavaScript existen cinco (5) tipos de datos primitivos:

1)String(cadena)

2)Number(número)

3)Boolean(booleano)

4)Undefined(indefinido)

5)Null(nulo)

### 1)Variable de Tipo String:

Es una cadena de texto. Para indicar que la variable es una cadena, debes  escribirlo entre comillas simples o dobles.

```javascript
var miVariable1 = 'Estoy aprendiendo desarrollo web';
var miVariable2 = "Javascript es un lenguaje de Programación.";
var miVariable3 = '2018';
var miVariable4 = '04-08-2018';
```

### 2)Variable de Tipo Number:

Es un número. Los números no tienen comillas.

```javascript
var miVariable5 = 10;
```

### 3)Variable de Tipo Booblean:

Tienen valor verdadero/falso. `true/false` son palabras especiales en JS, y no necesitan comillas.

```javascript
var miVariable6 = true;
```

### 4)Variable del tipo `Undefined`:

Es una variable a la que no se le ha asignado un valor, es decir tiene el valor indefinido.

### 5)Variable del tipo `Null`:

El tipo Nulo tiene exactamente un valor: nulo.El valor `null` representa la ausencia intencional de cualquier valor de objeto.

### Atención:

Las variables `Null` y `Undefined` indican la ausencia de valor, sin embargo puede diferenciarse en que  para el Tipo `Null` hay una intención en esta ausencia y en el otro no (`undefined`).

---

### Operadores:

Asi como hay variables, tambien tenemos operadores que son elementos importantes dentro del Lenguaje Javascript y todos los lenguajes de programación:

1)Suma/concatenación

2)Resta, multiplicación, división

3)Operador de asignación

4)Identidad/igualdad

5)Negación, distinto (no igual)

### 1)Suma/concatenación:

Se usa para sumar dos números, o juntar dos cadenas en una. Su simbolo es: +

```javascript
6 + 9;
"Hola " + "mundo!";
```

### 2)Resta, multiplicación, división:

Éstos hacen lo que esperarías que hicieran en las matemáticas básicas. Sus simbolos son: -, *, /

```javascript
9 - 3;
8 * 2; // La multiplicación en JS es un asterisco
9 / 3;
```

### 3)Operador de asignación:

Los hemos visto anteriormente: asigna un valor a una variable. Su simbolo es: =

```javascript
nombreDeLaVariable = 'capacitadero';
```

### 4)Identidad/igualdad:

Comprueba si dos valores son iguales entre sí, y devuelve un valor de true/false (booleano). Su simbolo es: ===

```javascript
var miVariable = 3;
miVariable === 4;
```

### 5)Negación, distinto(no igual):

En ocasiones utilizado con el operador de identidad, la negación es en JS el equivalente al operador lógico NOT — cambia true por false y viceversa.
Su simbolo es: !, !==

En el siguiente ejemplo, la expresión básica es true, pero la comparación devuelve false porque lo hemos negado:

```javascript
var miVariable = 3;
!miVariable === 3;
```

Aquí abajo estamos comprobando "es miVariable NO igual a 3". Esto devuelve false, porque ES igual a 3.

```javascript
var miVariable = 3;
miVariable !== 3;
```
---

Recursos Adicionales:
--------------------

Parte del texto del Proceso fue extraido de la organización [Mozilla](https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/JavaScript_basics
)

Información adicional que es necesario leer:
* https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Expressions_and_Operators